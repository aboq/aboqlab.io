import React, { useState, useContext, Dispatch, SetStateAction } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Security, SecureRoute, Auth } from "@okta/okta-react";
import logo from "./boq.png";
import logodark from "./boq-dark.png";
import "./App.css";

import ImplicitCallback from "utils/ImplicitCallback";

import BookView from "./views/BookView";

import {
  Button,
  Navbar,
  Alignment,
  Classes,
  Switch as BPSwitch,
  Icon,
  ControlGroup,
  H3,
  ResizeSensor,
  IResizeEntry
} from "@blueprintjs/core";
import { FocusStyleManager } from "@blueprintjs/core";
import AdminView from "views/AdminView";
import { createBrowserHistory } from "history";
import { UserContext } from "utils/UserContext";
import { ThemeContext } from "utils/ThemeContext";
import Width from "models/Width";
import { WidthContext } from "utils/WidthContext";

FocusStyleManager.onlyShowFocusOnTabs();
const CLIENT_ID = "0oa240i5p0afvoWqV357";
const ISSUER = `https://dev-936866.okta.com/oauth2/default`;
const HOST = window.location.host;
const REDIRECT_URI = `http://${HOST}/implicit/callback`;
const SCOPES = "openid profile email";
const okta_config = {
  issuer: ISSUER,
  clientId: CLIENT_ID,
  redirectUri: REDIRECT_URI,
  scopes: SCOPES.split(/\s+/),
  pcke: true
};

const auth = new Auth({
  history: createBrowserHistory(),
  ...okta_config
});

interface IUser {
  email?: string;
  email_verified?: boolean;
  family_name?: string;
  given_name?: string;
  locale?: string;
  name?: string;
  preferred_username?: string;
  sub?: string;
  updated_at?: number;
  zoneinfo?: string;
}

interface IUserContext {
  accessToken: string;
  user: IUser;
}

const ThemeSwitch: React.FC<{
  useDarkTheme: boolean;
  setUseDarkTheme: Dispatch<SetStateAction<boolean>>;
  compact: boolean;
}> = ({ useDarkTheme, setUseDarkTheme, compact }) => {
  if (compact) {
    return (
      <Button
        icon={
          <Icon
            icon={useDarkTheme ? "moon" : "flash"}
            iconSize={Icon.SIZE_LARGE}
            color="palegoldenrod"
          />
        }
        minimal={true}
        onClick={() => setUseDarkTheme(!useDarkTheme)}
      />
    );
  }

  return (
    <ControlGroup
      style={{
        display: "flex",
        alignItems: "center",
        marginRight: 10
      }}
    >
      <Icon
        icon="flash"
        iconSize={Icon.SIZE_LARGE}
        style={{ marginRight: 10 }}
        color={useDarkTheme ? undefined : "palegoldenrod"}
      />
      <BPSwitch
        style={{ margin: 0 }}
        inline={true}
        large={true}
        checked={useDarkTheme}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setUseDarkTheme(e.target.checked)
        }
      />
      <Icon
        icon="moon"
        iconSize={Icon.SIZE_LARGE}
        color={useDarkTheme ? "palegoldenrod" : undefined}
      />
    </ControlGroup>
  );
};

const Hide: React.FC<{ threshold: Width }> = ({ threshold, children }) => {
  let width = useContext(WidthContext);
  if (width <= threshold) {
    return null;
  }

  return <>{children}</>;
};

const App: React.FC<{ setAccessToken: () => void }> = ({ setAccessToken }) => {
  let [useDarkTheme, setUseDarkTheme] = useState(true);
  let [width, setWidth] = useState<number>(Width.MEDIUM);
  let widthThresholds = [
    Width.SMALL,
    Width.SMALL2,
    Width.MEDIUM,
    Width.LARGE,
    Width.XL
  ];

  let { user, setUser } = useContext<any>(UserContext);

  const viewContainerStyles = {
    display: "flex",
    justifyContent: "center",
    background: useDarkTheme ? "#293742" : "#EBF1F5"
  };

  return (
    <ThemeContext.Provider value={useDarkTheme}>
      <WidthContext.Provider value={width}>
        <Router>
          <Navbar className={Classes.DARK} fixedToTop={true}>
            <Navbar.Group align={Alignment.LEFT}>
              <Navbar.Heading>
                {width <= Width.MEDIUM ? "BoQ" : "Book of the Quarter"}
              </Navbar.Heading>
              <Navbar.Divider />
              <Link to="/">
                <Button minimal={true} icon="home">
                  <Hide threshold={Width.SMALL}>Home</Hide>
                </Button>
              </Link>
              <Link to="/books">
                <Button minimal={true} icon="book">
                  <Hide threshold={Width.SMALL}>Books</Hide>
                </Button>
              </Link>
              <Link to="/admin">
                <Button minimal={true} icon="application">
                  <Hide threshold={Width.SMALL}>Admin</Hide>
                </Button>
              </Link>
              <a
                href="https://gitlab.com/aboq"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Button className="bp3-minimal" icon="link">
                  <Hide threshold={Width.SMALL}>Edit this site</Hide>
                </Button>
              </a>
            </Navbar.Group>
            <Navbar.Group align={Alignment.RIGHT}>
              <ThemeSwitch
                useDarkTheme={useDarkTheme}
                setUseDarkTheme={setUseDarkTheme}
                compact={width <= Width.MEDIUM}
              />
              <Button minimal={true}>
                {user &&
                  (width <= Width.SMALL2
                    ? user.name
                        .split(" ")
                        .map((n: string) => n[0])
                        .join("")
                    : user.name)}
              </Button>
            </Navbar.Group>
          </Navbar>

          <div
            style={viewContainerStyles}
            className={useDarkTheme ? Classes.DARK : ""}
          >
            <ResizeSensor
              onResize={(e: IResizeEntry[]) => {
                let widthGroup =
                  widthThresholds.find(
                    widthMax => widthMax >= e[0].target.clientWidth
                  ) || 0;
                setWidth(widthGroup);
              }}
            >
              <Security auth={auth}>
                <RouteView path="/books" component={BookView} />
                <RouteView path="/admin" component={AdminView} />
                <RouteView path="/" exact={true} component={Home} />
                <Route path="/implicit/callback">
                  <ImplicitCallback
                    auth={auth}
                    setUser={setUser}
                    setAccessToken={setAccessToken}
                  />
                </Route>
              </Security>
            </ResizeSensor>
          </div>
        </Router>
      </WidthContext.Provider>
    </ThemeContext.Provider>
  );
};

const RouteView: React.FC<any> = ({ component: Component, ...props }) => {
  const WrappedView = () => (
    <div className="view-wrapper">
      <div className="view">
        <Component />
      </div>
    </div>
  );

  return <SecureRoute {...props} component={WrappedView} />;
};

const Home = () => {
  const useDarkTheme = React.useContext(ThemeContext);

  return (
    <div className="App">
      <header className="App-header">
        <img
          className="logo"
          src={useDarkTheme ? logodark : logo}
          alt="Book of the Quarter Logo"
        />
      </header>

      <H3>Program Overview</H3>
      <p>
        Here's your chance to build your skills, expand your network and improve
        yourself, as well as get a free book and six free lunches each quarter.
        Look at the list of Books, below, to see if there is a book you would
        like to read during the next quarter. If you know of a work-related book
        that you would rather read, or that you think would be good for others
        to read, please add it to the list. Groups will meet six times during
        the quarter, generally once every other week, to discuss what they have
        read since the previous meeting. Meetings will generally be held on site
        from 12 PM to 1 PM, although your group can make other arrangements.
        Lunches will be provided as lunch vouchers. You may only sign up for one
        book each quarter. It is expected that the majority of your reading
        should be completed outside of normal business hours. When the quarter
        is over, the book is yours to keep and take with you if/when you leave
        Adobe.
      </p>

      <H3>Sign up</H3>
      <p>
        About three weeks before the start of a new quarter, sign-ups will close
        for that quarter. At that time, any book that has at least four people
        signed up will be selected as a book for that quarter. If the book that
        you signed up for did not get at least four people to sign up, you will
        have a couple of days to add your name to the list of one of the books
        that made the cut, before they are ordered. You can then sign up for
        your original choice for the subsequent quarter and try to recruit more
        people to sign up for it then. If you do not add your name before the
        books are ordered, you may still join with that group, but you will be
        responsible for ordering the book yourself and filing an expense report
        to be reimbursed.
      </p>

      <H3>Group Leaders</H3>
      <p>
        One member of each group will be designated as the leader. The group
        leader will divide up the book into six reading assignments, and assign
        a different member of the group to lead each of the discussions. The
        group leader will then reserve a room, and send out a meeting request to
        each team member for each of these six dates. The meeting request should
        include the reading schedule and discussion leader assignments. If the
        assigned discussion leader is unable to attend the meeting they are
        responsible to arrange with another person to lead the discussion.
      </p>

      <H3>Sponsorship</H3>
      <p>
        The BoQ program is sponsored by Ryan Packer and is therefore currently
        limited to members of his organization. If you would like to have this
        program expanded to your organization or if you have any other
        questions, please contact the{" "}
        <a href="mailto:boq-admins@adobe.com">BoQ Admins</a>.
      </p>
    </div>
  );
};

export default App;
