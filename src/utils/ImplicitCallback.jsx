import React, { Component } from "react";
import { Redirect } from "react-router";

export default class ImplicitCallback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: null,
      error: null
    };
  }

  componentDidMount() {
    this.props.auth
      .handleAuthentication()
      .then(() => this.setState({ authenticated: true }))
      .catch(err =>
        this.setState({ authenticated: false, error: err.toString() })
      );
  }

  async componentWillUnmount() {
    let user = await this.props.auth.getUser();
    let accessToken = await this.props.auth.getAccessToken();
    this.props.setUser(user);
    this.props.setAccessToken(accessToken);
    localStorage.setItem("boq-access-token", accessToken);
    localStorage.setItem("boq-user", JSON.stringify(user));
  }

  render() {
    if (this.state.authenticated === null) {
      return null;
    }

    const referrerKey = "secureRouterReferrerPath";
    const location = JSON.parse(
      localStorage.getItem(referrerKey) || '{ "pathname": "/" }'
    );
    localStorage.removeItem(referrerKey);

    return this.state.authenticated ? (
      <Redirect to={location} />
    ) : (
      <p>{this.state.error}</p>
    );
  }
}
