import * as React from "react";
import {
  FormGroup,
  ControlGroup,
  Button,
  Intent,
  InputGroup,
  Dialog,
  Classes,
  Toaster,
  TextArea,
  NumericInput
} from "@blueprintjs/core";
import { BookRepository } from "models/Book";
import FormatEnum from "models/Format";
import { ThemeContext } from "utils/ThemeContext";

interface Format {
  [k: string]: any;

  available: boolean;
  link: string | null;
}
interface IFormatStates {
  [k: string]: Format;
}

interface FormatProps {
  formats: IFormatStates;
  setFormatLink: (format: string, link: string) => void;
  toggleFormat: (format: string) => void;
}

const bookFormReducer = (state: any, action: any) => {
  const isAvailable = (format: string) => {
    return state.formats[format] && state.formats[format].available;
  };

  let { formats } = state;

  switch (action.type) {
    case "TOGGLE_AVAILABILITY":
      formats[action.format] = {
        ...formats[action.format],
        available: !isAvailable(action.format)
      };

      return {
        ...state,
        formats
      };
    case "CHANGE_FORMAT_LINK":
      formats[action.format] = {
        ...formats[action.format],
        link: action.value
      };

      return {
        ...state,
        formats
      };
    case "CHANGE_TITLE":
      return {
        ...state,
        title: action.value
      };
    case "CHANGE_DESCRIPTION":
      return {
        ...state,
        description: action.value
      };
    case "CHANGE_PAGES":
      return {
        ...state,
        pages: action.value
      };
    case "CHANGE_PUBLISH_YEAR":
      return {
        ...state,
        publishYear: action.value
      };
    case "CHANGE_AUTHORS":
      return {
        ...state,
        authors: action.value.split(/,\s*/)
      };
    case "CHANGE_TAGS":
      return {
        ...state,
        tags: action.value.split(/,\s*/)
      };

    default:
      throw new Error("not implemented");
  }
};

const setDangerIntentIf = (cond: any): Intent => {
  return cond ? Intent.DANGER : Intent.NONE;
};

const FormatsFormGroup: React.FC<FormatProps> = ({
  formats,
  setFormatLink,
  toggleFormat
}) => {
  let formatTitleStyle = {
    width: 100
  };

  let errors: FormError[] = React.useContext(FormErrorContext);
  let formatOptions: FormatEnum[] = [
    FormatEnum.BOOK,
    FormatEnum.AUDIBLE,
    FormatEnum.KINDLE,
    FormatEnum.SAFARI
  ];

  const isAvailable = (format: string) => {
    return formats[format] && formats[format].available;
  };

  return (
    <FormGroup
      helperText="Click on format types to toggle availability"
      label="Formats"
      labelInfo="(required)"
      intent={setDangerIntentIf(
        errors.find(error => error.field === "formats" && !error.subField)
      )}
    >
      {formatOptions.map(format => (
        <FormGroup key={format}>
          <ControlGroup>
            <Button
              style={formatTitleStyle}
              intent={isAvailable(format) ? Intent.SUCCESS : Intent.NONE}
              onClick={() => toggleFormat(format)}
            >
              {format}
            </Button>
            <InputGroup
              fill={true}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setFormatLink(format, e.target && e.target.value)
              }
              intent={setDangerIntentIf(
                errors.find(error => error.subField === format)
              )}
            />
          </ControlGroup>
        </FormGroup>
      ))}
    </FormGroup>
  );
};

const BookFormBody: React.FC<{ state: any; dispatch: any }> = ({
  state,
  dispatch
}) => {
  let errors: FormError[] = React.useContext(FormErrorContext);

  return (
    <div className={Classes.DIALOG_BODY}>
      <FormGroup label="Title" labelFor="title" labelInfo="(required)">
        <InputGroup
          id="title"
          name="title"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "CHANGE_TITLE",
              value: e.target && e.target.value
            })
          }
          intent={setDangerIntentIf(
            errors.find(error => error.field === "title")
          )}
        />
      </FormGroup>

      <FormGroup
        label="Description"
        labelFor="description"
        labelInfo="(required)"
      >
        <TextArea
          id="description"
          name="description"
          fill={true}
          growVertically={true}
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
            dispatch({
              type: "CHANGE_DESCRIPTION",
              value: e.target && e.target.value
            })
          }
          intent={setDangerIntentIf(
            errors.find(error => error.field === "description")
          )}
        />
      </FormGroup>

      <div style={{ display: "grid", gridTemplateAreas: "'page year'" }}>
        <FormGroup
          style={{ gridArea: "page" }}
          label="Page Count"
          labelFor="pages"
          labelInfo=""
        >
          <NumericInput
            id="pages"
            name="pages"
            min={0}
            max={9999}
            size={4}
            selectAllOnFocus={true}
            value={0}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              dispatch({
                type: "CHANGE_PAGES",
                value: e.target && e.target.value
              })
            }
          />
        </FormGroup>
        <FormGroup
          style={{ gridArea: "year" }}
          label="Year Published"
          labelFor="publishYear"
          labelInfo=""
        >
          <NumericInput
            id="publishYear"
            name="publishYear"
            min={1900}
            max={2100}
            size={4}
            selectAllOnFocus={true}
            value={2018}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              dispatch({
                type: "CHANGE_PUBLISH_YEAR",
                value: e.target && e.target.value
              })
            }
          />
        </FormGroup>
      </div>

      <FormGroup
        label="Authors"
        labelFor="authors"
        labelInfo="(comma separated)"
      >
        <InputGroup
          id="authors"
          name="authors"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "CHANGE_AUTHORS",
              value: e.target && e.target.value
            })
          }
        />
      </FormGroup>

      <FormGroup label="Tags" labelFor="tags" labelInfo="(comma separated)">
        <InputGroup
          id="tags"
          name="tags"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "CHANGE_TAGS",
              value: e.target && e.target.value
            })
          }
        />
      </FormGroup>

      <FormatsFormGroup
        formats={state.formats}
        setFormatLink={(format, link) =>
          dispatch({ type: "CHANGE_FORMAT_LINK", format, value: link })
        }
        toggleFormat={format =>
          dispatch({ type: "TOGGLE_AVAILABILITY", format })
        }
      />
    </div>
  );
};

const BookFormFooter: React.FC<{
  onCancel: () => void;
  onSubmit: any;
}> = ({ onCancel, onSubmit }) => {
  return (
    <div className={Classes.DIALOG_FOOTER}>
      <div className={Classes.DIALOG_FOOTER_ACTIONS}>
        <Button onClick={onCancel}>Cancel</Button>
        <Button
          onClick={onSubmit}
          intent={Intent.PRIMARY}
          type="submit"
          // disabled={Object.values(formats)}
        >
          Submit
        </Button>
      </div>
    </div>
  );
};
interface BookFormProps {
  isOpen: boolean;
  onClose: () => void;
  onCancel: () => void;
  onSubmit: () => void;
}

interface FormError {
  field: string;
  subField?: string;
  message: string;
}

const validateForm = (state: IFormatStates): FormError[] => {
  let errors: FormError[] = [];
  let { title, description, formats } = state;
  if (!title) {
    errors.push({ field: "title", message: "Title is required" });
  }

  if (!description) {
    errors.push({ field: "description", message: "Description is required" });
  }

  let values: Format[] = Object.values(formats);

  // No formats
  if (!values.length) {
    errors.push({
      field: "formats",
      message: "At least one format must be available"
    });
  }

  // No formats set to available
  if (!values.some(value => value.available)) {
    errors.push({
      field: "formats",
      message: "At least one format must be available"
    });
  }

  let entries = Object.entries(formats);
  entries.forEach(([key, value]) => {
    // Available but no link
    if (value.available && !value.link) {
      errors.push({
        field: "formats",
        subField: key,
        message: "A link is required for a selected format"
      });
    }

    // If not available, we don't care about the link
  });

  return errors;
};

const FormErrorContext = React.createContext<FormError[]>([]);
let toaster = Toaster.create();

const BookForm: React.FC<BookFormProps> = ({
  isOpen,
  onClose,
  onCancel,
  onSubmit
}) => {
  const [state, dispatch] = React.useReducer(bookFormReducer, {
    formats: {}
  });
  const useDarkTheme = React.useContext(ThemeContext);
  const [errors, setErrors] = React.useState<FormError[]>([]);
  const submitForm = () => {
    let errors = validateForm(state);
    if (errors.length) {
      setErrors(errors);
    } else {
      BookRepository.save(state).then(data => {
        toaster.show({
          message: "Book submitted",
          intent: Intent.SUCCESS,
          icon: "tick-circle"
        });
        onSubmit();
      });
      // onSubmit();
    }
  };
  return (
    <Dialog
      isOpen={isOpen}
      usePortal={true}
      canOutsideClickClose={true}
      canEscapeKeyClose={true}
      title="Suggest A Book"
      onClose={onClose}
      className={useDarkTheme ? Classes.DARK : ""}
    >
      <FormErrorContext.Provider value={errors}>
        <BookFormBody state={state} dispatch={dispatch} />
        <BookFormFooter onCancel={onCancel} onSubmit={submitForm} />
      </FormErrorContext.Provider>
    </Dialog>
  );
};

export default BookForm;
