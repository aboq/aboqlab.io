import React, { useContext } from "react";
import {
  Tag,
  Colors,
  Dialog,
  Classes,
  H4,
  Icon,
  H5,
  Text,
  Popover,
  PopoverInteractionKind,
  Collapse
} from "@blueprintjs/core";
import { Button, Card, Elevation, Intent } from "@blueprintjs/core";
import Subscribe from "components/core/Subscribe";
import { ThemeContext } from "utils/ThemeContext";
import shave from "shave";
import { WidthContext } from "utils/WidthContext";
import Width from "models/Width";
import { UserContext } from "utils/UserContext";
import { gql } from "apollo-boost";
import { client } from "models/App";

function Shave({
  children,
  className,
  maxHeight,
  element = "div",
  enabled = true
}) {
  // keep track of the DOM element to shave
  let elemRef = React.useRef();
  // Allow passing in which dom element to use
  let Element = element;

  // The effect will run anytime maxHeight or enabled changes
  React.useEffect(() => {
    // Only shave if we are supposed to
    if (enabled) {
      shave(elemRef.current, maxHeight);
    }
  }, [maxHeight, enabled]);

  // By using enabled as our 'key', we force react to create a
  // completely new DOM node if enabled changes.
  return (
    <Element key={enabled} ref={elemRef} className={className}>
      {children}
    </Element>
  );
}

const ParticipantsBox = ({ quarter, book, refetch, compact, isExpanded }) => {
  const useDarkTheme = useContext(ThemeContext);

  let users = book.users || [];
  let [participants, setParticipants] = React.useState(
    users.filter(user => user.quarter === quarter)
  );
  let [isOpen, setIsOpen] = React.useState(false);
  let [willLead, setWillLead] = React.useState(true);
  let [priority, setPriority] = React.useState(1);
  let [loc, setLocation] = React.useState("Lehi");
  let [format, setFormat] = React.useState("BOOK");
  let { user } = React.useContext(UserContext);
  let username = user.email.substring(0, user.email.indexOf("@"));
  let [btnState, setBtnState] = React.useState(joinOrLeave());

  function setWillLeadLog(value) {
    setWillLead(value);
    console.log(willLead);
  }
  function setPriorityLog(value) {
    setPriority(value);
    console.log(priority);
  }
  function setFormatLog(value) {
    setFormat(value);
    console.log(format);
  }
  function setLocationLog(value) {
    setLocation(value);
    console.log(loc);
  }

  async function unsub() {
    const UNSUB = gql`
      mutation unsubscribe($id: ID, $user: UserInput) {
        unsubscribe(user: $user, id: $id) {
          username
        }
      }
    `;
    let myUser = {};
    myUser.format = format;
    myUser.location = loc;
    myUser.priority = priority;
    myUser.quarter = quarter;
    myUser.username = username;
    myUser.will_lead = willLead;
    const myId = book.id;

    client.mutate({
      mutation: UNSUB,
      variables: { id: myId, user: myUser }
    });

    setParticipants(participants.filter(u => u.username !== username));

    return;
  }

  async function dosub() {
    const SUB = gql`
      mutation subscribe($id: ID, $user: UserInput) {
        subscribe(user: $user, id: $id) {
          username
        }
      }
    `;
    let myUser = {};
    myUser.format = format;
    myUser.location = loc;
    myUser.priority = priority;
    myUser.quarter = quarter;
    myUser.username = username;
    myUser.will_lead = willLead;
    myUser.name = username;
    const myId = book.id;

    client.mutate({
      mutation: SUB,
      variables: { id: myId, user: myUser }
    });

    participants.push(myUser);
    setParticipants(participants);

    return;
  }

  function closeAndSubmit() {
    dosub().then(setBtnState("Leave Group"));
    setIsOpen(false);
  }
  function joinOrLeave() {
    if (participants.find(user => username === user.username)) {
      return "Leave Group";
    } else {
      return "Join Group";
    }
  }

  function joinOrLeaveCallback() {
    if (joinOrLeave() === "Join Group") {
      return setIsOpen(true);
    } else {
      unsub().then(setBtnState("Join Group"));
    }
  }
  return (
    <div className="bookcard-secondary">
      <NameList participants={participants} quarter={quarter}>
        <Button
          intent={Intent.PRIMARY}
          text={btnState}
          onClick={() => joinOrLeaveCallback()}
        />
        <Dialog
          isOpen={isOpen}
          usePortal={true}
          canOutsideClickClose={true}
          canEscapeKeyClose={true}
          title="Subscribe"
          onClose={() => setIsOpen(false)}
          style={{ width: "525px" }}
          className={useDarkTheme ? Classes.DARK : ""}
        >
          <div className={Classes.DIALOG_BODY}>
            <Subscribe
              book={book}
              willLead={setWillLeadLog}
              priority={setPriorityLog}
              format={setFormatLog}
              loc={setLocationLog}
            />
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <Button onClick={() => setIsOpen(false)}>Cancel</Button>
            <Button
              onClick={closeAndSubmit}
              intent={Intent.PRIMARY}
              type="submit"
            >
              Submit
            </Button>
          </div>
        </Dialog>
      </NameList>
    </div>
  );
};

const BookDetails = ({
  year,
  pages,
  authors,
  tags,
  description,
  compact,
  isExpanded
}) => {
  if (compact && !isExpanded) {
    return null;
  }

  return (
    <>
      <div className="book-description">
        <Shave maxHeight={110}>{description}</Shave>
      </div>
      <div className="book-details">
        <>
          <div className="book-detail-row">
            <div className="book-icon-detail">
              <Icon style={{ marginRight: 5 }} icon="calendar" />
              {year}
            </div>
            <div className="book-icon-detail">
              <Icon style={{ marginRight: 5 }} icon="document" />
              {pages}
            </div>
            <div className="book-icon-detail">
              <Popover interactionKind={PopoverInteractionKind.HOVER}>
                <Icon
                  intent={tags.length ? Intent.PRIMARY : Intent.NONE}
                  style={{ marginRight: 5 }}
                  icon="tag"
                />
                <div style={{ padding: 20 }}>{tags}</div>
              </Popover>
              {tags.length}
            </div>
            <Text
              ellipsize={true}
              className="book-icon-detail"
              style={{ flexGrow: 0 }}
            >
              <Icon style={{ marginRight: 5 }} icon="edit" />
              {authors}
            </Text>
          </div>
        </>
      </div>
    </>
  );
};

const BookCard = props => {
  function getField(filed) {
    return props.book[filed];
  }

  function createTag(tag) {
    return (
      <Tag
        key={tag}
        round={true}
        style={{
          background: Colors.BLUE3,
          color: Colors.WHITE,
          marginRight: 5
        }}
      >
        {tag}
      </Tag>
    );
  }

  function getTags() {
    const tags = getField("tags") || [];
    return tags.map(createTag);
  }

  function getAuthors() {
    let authors = getField("authors") || [];
    const maxAuthors = 2;
    if (authors.length > maxAuthors) {
      authors[maxAuthors] = "et. al.";
      return authors.slice(0, 3);
    }
    return authors;
  }

  let [isExpanded, setIsExpanded] = React.useState(false);

  const width = useContext(WidthContext);
  let compact = width <= Width.SMALL2;
  const formats = ["Book", "Audible", "Kindle", "Safari"];

  const quarter = props.quarter;
  let availableFormats = props.book.formats.map(f => f.format);
  return (
    <Card
      interactive={compact ? true : false}
      elevation={Elevation.TWO}
      onClick={() => {
        if (compact) {
          setIsExpanded(!isExpanded);
        }
      }}
    >
      <div
        className={
          "bookcard" +
          (isExpanded ? " expanded" : "") +
          (compact ? " compact" : "")
        }
        style={{ height: isExpanded ? "100%" : undefined }}
      >
        <div className="bookcard-main">
          <div className="book-heading">
            <H4>
              <Shave maxHeight={63 /* line height 21 * 3 lines */}>
                {getField("title")}
              </Shave>
            </H4>
          </div>
          <div className="book-formats">
            {formats.map(format => (
              <Tag
                key={format}
                intent={
                  availableFormats.includes(format.toUpperCase())
                    ? Intent.SUCCESS
                    : Intent.NONE
                }
                style={{ marginRight: 5 }}
              >
                {format}
              </Tag>
            ))}
          </div>
          {compact ? null : (
            <BookDetails
              description={props.book.description}
              availableFormats={availableFormats}
              year={getField("publishYear") || "N/A"}
              pages={getField("pages") || "N/A"}
              authors={getAuthors().join(", ") || "N/A"}
              tags={getTags()}
            />
          )}
        </div>

        {compact ? (
          <Collapse isOpen={isExpanded} keepChildrenMounted={true}>
            <ParticipantsBox quarter={quarter} book={props.book} />
          </Collapse>
        ) : (
          <ParticipantsBox
            quarter={quarter}
            book={props.book}
            refetch={props.refetch}
          />
        )}
      </div>
    </Card>
  );
};

const NameItem = ({ user }) => {
  let itemStyle = {
    height: 25,
    display: "flex",
    alignItems: "center"
  };
  return (
    <div key={user.name} style={itemStyle}>
      <span>{user.name}</span>
    </div>
  );
};

const NameList = ({ participants, quarter, children }) => {
  const useDarkTheme = useContext(ThemeContext);
  return (
    <div
      className={"box" + (useDarkTheme ? " dark" : "")}
      style={{
        width: "100%",
        display: "flex",
        flexDirection: "column"
      }}
    >
      <div style={{ display: "flex", alignItems: "flex-start" }}>
        <H5 style={{ marginRight: 10 }}>Participants</H5>
        <Tag round={true}>{participants.length}</Tag>
      </div>
      <div style={{ paddingBottom: 10 }}>
        {participants.length
          ? participants.map(user => <NameItem key={user.name} user={user} />)
          : null}
      </div>
      {children}
    </div>
  );
};

export default BookCard;
