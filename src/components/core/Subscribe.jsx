import React from "react";
import {
  Switch,
  ButtonGroup,
  Button,
  NumericInput,
  Tooltip,
  Position,
  Icon,
  Intent
} from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

function capitalize(text) {
  return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
}

const Subscribe = props => {
  let [format, setFormat] = React.useState("BOOK");
  let [priority, setPriority] = React.useState(1);
  let [location, setLocation] = React.useState("Lehi");

  function handlePublicChange(e) {
    props.willLead(e.target.checked);
  }

  function handleFormatChange(f) {
    setFormat(f);
    props.format(f);
  }

  function handlePriorityChange(p) {
    setPriority(p);
    props.priority(p);
  }

  function handleLocationChange(l) {
    setLocation(l);
    props.loc(l);
  }

  const spanStyle = {
    verticalAlign: "middle"
  };

  const rowStyle = {
    padding: "10px"
  };

  return (
    <div>
      <div style={rowStyle}>
        <span style={spanStyle} className="bp3-text-large">
          Preferred format
        </span>
        <Tooltip
          content={
            <span>
              We will do our best to fullfil your request for your <br></br>
              preferred format. We will select a different format <br></br>
              if your preferred format is not available.
            </span>
          }
          position={Position.TOP_LEFT}
          usePortal={false}
          intent={Intent.PRIMARY}
        >
          <Icon
            icon={IconNames.HELP}
            intent={Intent.PRIMARY}
            style={{ padding: "0 10px" }}
          />
        </Tooltip>
        <ButtonGroup>
          {props.book.formats.map(item => (
            <Button
              active={item.format === format}
              key={item.format}
              onClick={() => handleFormatChange(item.format)}
            >
              {capitalize(item.format)}
            </Button>
          ))}
        </ButtonGroup>
      </div>
      <div style={rowStyle}>
        <span style={spanStyle} className="bp3-text-large">
          Your priority
        </span>
        <Tooltip
          content={
            <span>
              You can choose up to 3 books and place them in <br></br>priority
              order. 1 is the highest priority.
            </span>
          }
          position={Position.TOP_LEFT}
          usePortal={false}
          intent={Intent.PRIMARY}
        >
          <Icon
            icon={IconNames.HELP}
            intent={Intent.PRIMARY}
            style={{ padding: "0 10px" }}
          />
        </Tooltip>
        <div style={{ display: "inline-block" }}>
          <NumericInput
            buttonPosition="left"
            intent={Intent.PRIMARY}
            min={1}
            max={3}
            size={1}
            value={priority}
            onValueChange={handlePriorityChange}
          ></NumericInput>
        </div>
      </div>
      <div style={rowStyle}>
        <span style={spanStyle} className="bp3-text-large">
          Are you willing to lead the group?
        </span>
        <Tooltip
          content={
            <span>
              At a minimum leading a group means you will distribute <br></br>
              the books when they come in and schedule 6 meetings<br></br>
              over the next quarter.
            </span>
          }
          position={Position.TOP_LEFT}
          usePortal={false}
          intent={Intent.PRIMARY}
        >
          <Icon
            icon={IconNames.HELP}
            intent={Intent.PRIMARY}
            style={{ padding: "0 10px" }}
          />
        </Tooltip>
        <Switch
          innerLabel="No"
          innerLabelChecked="Yes"
          inline
          onChange={handlePublicChange}
        />
      </div>
      <div style={rowStyle}>
        <span style={spanStyle} className="bp3-text-large">
          Location
        </span>
        <Tooltip
          content={<span>In which office will your group meet?</span>}
          position={Position.TOP_LEFT}
          usePortal={false}
          intent={Intent.PRIMARY}
        >
          <Icon
            icon={IconNames.HELP}
            intent={Intent.PRIMARY}
            style={{ padding: "0 10px" }}
          />
        </Tooltip>
        <ButtonGroup>
          {["Lehi", "San Jose", "San Francisco", "New York", "Virginia"].map(
            item => (
              <Button
                active={item === location}
                key={item}
                onClick={() => handleLocationChange(item)}
              >
                {item}
              </Button>
            )
          )}
        </ButtonGroup>
      </div>
    </div>
  );
};

export default Subscribe;
