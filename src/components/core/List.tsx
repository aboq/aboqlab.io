import * as React from "react";
import { NonIdealState } from "@blueprintjs/core";

export interface Item {
  key: string;
}

interface ListProps {
  items: Array<Item>;
  itemRenderer: (item: any) => JSX.Element[] | JSX.Element;
}

interface ListItemProps {
  children: JSX.Element[] | JSX.Element;
}

const listItemStyle = {
  margin: "10px 0px"
};

const ListItem: React.FC<ListItemProps> = ({ children }) => {
  return (
    <div style={listItemStyle} className="list-item">
      {children}
    </div>
  );
};

const List: React.FC<ListProps> = ({ itemRenderer, items }) => {
  if (items.length) {
    return (
      <div>
        {items.map(item => (
          <ListItem key={item.key}>{itemRenderer(item)}</ListItem>
        ))}
      </div>
    );
  } else {
    return (
      <div>
        <NonIdealState
          icon="search"
          title="No results"
          description="Please modify your search query"
        />
      </div>
    );
  }
};

export default List;
