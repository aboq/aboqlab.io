import * as React from "react";

import List from "components/core/List";

import { useQuery } from "@apollo/react-hooks";
import {
  Spinner,
  Intent,
  ButtonGroup,
  Button,
  InputGroup,
  ControlGroup
} from "@blueprintjs/core";
import { Book, BookRepository, IBook } from "models/Book";
import BookCard from "components/core/Card";
import BookForm from "components/aux/BookForm";
import { ThemeContext } from "utils/ThemeContext";

interface BookListProps {
  books: IBook[];
  quarter: string;
  refetch: (options?: any) => Promise<any>;
}

const bookItemRenderer = (
  quarter: string,
  refetch: (options?: any) => Promise<any>
) => (book: Book) => (
  <BookCard book={book} quarter={quarter} refetch={refetch} />
);

const BookList: React.FC<BookListProps> = ({ books, quarter, refetch }) => {
  let bookItems = books.map((book, index) => {
    return { ...book, key: book.title + index };
  });
  return (
    <List items={bookItems} itemRenderer={bookItemRenderer(quarter, refetch)} />
  );
};

const BookListContainer: React.FC<{
  books: IBook[];
  refetch: (options?: any) => Promise<any>;
}> = ({ books, refetch }) => {
  let quarters = ["4Q2019", "1Q2020"];
  let [quarter, setQuarter] = React.useState<string>(quarters[0]);

  const [isOpen, setIsOpen] = React.useState(false);

  let controlStyles = {
    display: "flex",
    justifyContent: "space-between"
  };

  return (
    <>
      <div className="control-container" style={controlStyles}>
        <ButtonGroup>
          {quarters.map(_quarter => (
            <Button
              key={_quarter}
              onClick={() => {
                setQuarter(_quarter);
              }}
              active={quarter === _quarter}
            >
              {_quarter}
            </Button>
          ))}
        </ButtonGroup>
        <ControlGroup>
          <ButtonGroup style={{ justifySelf: "start" }} minimal={true}>
            <Button icon="add" onClick={() => setIsOpen(true)}>
              Add
            </Button>
            {isOpen ? (
              <BookForm
                isOpen={isOpen}
                onClose={() => setIsOpen(false)}
                onSubmit={() => {
                  setIsOpen(false);
                  refetch();
                }}
                onCancel={() => setIsOpen(false)}
              />
            ) : null}
          </ButtonGroup>
        </ControlGroup>
      </div>
      <BookList books={books} quarter={quarter} refetch={refetch} />
    </>
  );
};

const View: React.FC<{}> = () => {
  const useDarkTheme = React.useContext(ThemeContext);

  let [searchText, setSearchText] = React.useState("");

  // ? How can we consolidate the queries for the 2 views?
  const { loading, data, refetch } = useQuery(BookRepository.BOOKS_QUERY, {
    notifyOnNetworkStatusChange: true
  });

  const onSubmitSearch = () => {
    refetch({ query: searchText });
  };

  return (
    <div className={"box" + (useDarkTheme ? " dark" : "")}>
      <div style={{ marginBottom: 30 }}>
        <InputGroup
          type="search"
          placeholder="Search"
          leftIcon="search"
          rightElement={
            <Button
              icon="arrow-right"
              minimal={true}
              onClick={onSubmitSearch}
            />
          }
          value={searchText}
          onKeyDown={(e: React.KeyboardEvent) => {
            if (e.keyCode === 13) {
              onSubmitSearch();
            }
          }}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setSearchText(e.target.value)
          }
          round={true}
        />
      </div>

      {loading ? (
        <div
          style={{
            minHeight: 200,
            display: "flex",
            justifyContent: "center"
          }}
        >
          <Spinner intent={Intent.PRIMARY} />
        </div>
      ) : (
        <BookListContainer
          books={data.books.filter(BookRepository.validate)}
          refetch={refetch}
        />
      )}
    </div>
  );
};

export default View;
