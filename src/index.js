import React, { useState } from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import initializeApolloClient from "utils/initializeApolloClient";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";

import { FocusStyleManager } from "@blueprintjs/core";
import { UserContext } from "utils/UserContext";
FocusStyleManager.onlyShowFocusOnTabs();

const Root = () => {
  let [user, setUser] = useState(
    JSON.parse(localStorage.getItem("boq-user") || "{}")
  );

  let [accessToken, setAccessToken] = useState(
    localStorage.getItem("boq-access-token")
  );

  let client = initializeApolloClient(accessToken);

  return (
    <ApolloProvider client={client}>
      <UserContext.Provider value={{ user, setUser }}>
        <App setAccessToken={setAccessToken} />
      </UserContext.Provider>
    </ApolloProvider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
