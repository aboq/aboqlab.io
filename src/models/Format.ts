enum Format {
  BOOK = "BOOK",
  KINDLE = "KINDLE",
  AUDIBLE = "AUDIBLE",
  SAFARI = "SAFARI"
}

export default Format;
