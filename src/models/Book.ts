import { gql, ExecutionResult, FetchResult } from "apollo-boost";
import { client } from "./App";
import uuid from "uuid/v4";

export interface BookLink {
  format: string;
  link: string;
}

export interface IUser {
  username: string;
  name: string;
  quarter: string;
  priority: number;
  will_lead: boolean;
  location: string;
  format: string;
}
export interface IBook {
  id: string;
  title: string;
  description?: string;
  pages?: number;
  authors?: string[];
  publishYear?: number;
  tags?: string[];
  formats: BookLink[];
  users: IUser[];
}

export class Book implements IBook {
  [key: string]: any;
  id: string = "";
  title: string = "New Title";
  description?: string = "Book Description";
  pages?: number = 0;
  authors?: string[] = [];
  publishYear?: number = 1900;
  tags?: string[] = [];
  formats: BookLink[] = [];
  users = [];

  constructor(data: Book) {
    for (const prop in data) {
      if (data.hasOwnProperty(prop)) {
        this[prop] = data[prop];
      }
    }
  }
}

export class BookRepository {
  static BOOKS_QUERY = gql`
    query books($query: String) {
      books(query: $query) {
        id
        title
        description
        authors
        tags
        publishYear
        pages
        users {
          username
          name
          quarter
          priority
          will_lead
          location
          format
        }
        formats {
          format
        }
      }
    }
  `;

  static query(query: string): Promise<ExecutionResult> {
    return client.query({
      query: this.BOOKS_QUERY,
      variables: { query }
    });
  }

  static create(data: Book): Book {
    return new Book(data);
  }

  static transformForBackend(book: any): IBook {
    if (!book.id) {
      book.id = uuid();
    }

    let formats: BookLink[] = [];

    for (let format in book.formats) {
      if (book.formats[format].available) {
        formats.push({ format, link: book.formats[format].link });
      }
    }

    book.formats = formats;
    book.users = book.users || [];
    book.tags = book.tags || [];
    book.authors = book.authors || [];

    return book;
  }

  static async save(_book: any): Promise<FetchResult> {
    const SAVE_BOOK = gql`
      mutation SaveBook($book: BookInput!) {
        book(book: $book) {
          id
          title
        }
      }
    `;

    let book = this.transformForBackend(_book);

    return client.mutate({
      mutation: SAVE_BOOK,
      variables: { book }
    });
  }

  static validate(data: Book) {
    const requiredProps = ["title"];
    return requiredProps.every(prop => data[prop]);
  }
}
