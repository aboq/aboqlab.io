enum Width {
  SMALL = 550,
  SMALL2 = 600,
  MEDIUM = 800,
  LARGE = 1024,
  XL = 10000
}

export default Width;
